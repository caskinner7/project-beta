import React, { useState, useEffect } from "react";

function AutomobileList() {
    const [automobiles, setAutomobiles] = useState([]);


    const fetchAutomobiles = async () => {
        const automobilesUrl = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(automobilesUrl);

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos)
        }
    }


    const sold = (status) => {
        if (status === false) {
            return ("False")
        } else {
            return ("True")
        }
    }

    useEffect(() => {
        fetchAutomobiles();
    }, []);

    return (
        <>
            <h1>Automobiles</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold?</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles.map(auto => {
                        return (
                            <tr key={auto.id}>
                                <td>{auto.vin}</td>
                                <td>{auto.color}</td>
                                <td>{auto.year}</td>
                                <td>{auto.model.name}</td>
                                <td>{auto.model.manufacturer.name}</td>
                                <td>{sold(auto.sold)}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default AutomobileList;
