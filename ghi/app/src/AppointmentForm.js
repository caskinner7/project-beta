import React, {useEffect, useState} from 'react';

function AppointmentForm(){
  const [vin, setVin] = useState('');
  const [customer, setCustomer] = useState('');
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');
  const [tech, setTech] = useState('');
  const [reason, setReason] = useState('');
  const [technicians, setTechnicians] = useState([]);

    const handleSubmit = async(event)=> {
      event.preventDefault();
        const data={};
        data.vin = vin;
        data.customer = customer;
        data.date_time = date + " " + time;
        data.technician = tech;
        data.reason = reason;
        data.status = "created"

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            header: {
                'Content-Type': 'applications/json'
            },
        };

        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok){
            setVin('');
            setCustomer('');
            setDate('');
            setTime('');
            setTech('');
            setReason('');
        }
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value)
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value)
    }

    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value)
    }

    const handleTimeChange = (event) => {
        const value = event.target.value;
        setTime(value)
    }

    const handleTechChange = (event) => {
        const value = event.target.value;
        setTech(value)
    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value)
    }

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);

        if (response.ok){
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }

    useEffect(()=>{
        fetchData();
    }, []);

    return(
        <>
        <div className="container">
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a service appointment</h1>
                <form onSubmit={handleSubmit} id="create-appointment-form">
                  <div className="form-floating mb-3">
                    <input value={vin} onChange={handleVinChange} placeholder="Automobile Vin" required type="text" id="vin" name="vin" className="form-control"/>
                    <label htmlFor="vin">Automobile Vin</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={customer} onChange={handleCustomerChange} placeholder="Customer" required type="text" id="customer" name="customer" className="form-control"/>
                    <label htmlFor="customer">Customer</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={date} onChange={handleDateChange} placeholder="Date" required type="date" id="date_time" name="date_time" className="form-control"/>
                    <label htmlFor="date_time">Date</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={time} onChange={handleTimeChange} placeholder="Time" required type="time" id="date_time" name="date_time" className="form-control"/>
                    <label htmlFor="date_time">Time</label>
                  </div>
                  <div className="mb-3">
                    <select value={tech} onChange={handleTechChange} required id="technician" name="technician" className="form-select">
                      <option >Technician</option>
                      {technicians.map(technician=>{
                        return(
                            <option key={technician.employee_id} value={technician.id}>
                                {technician.first_name + " " + technician.last_name}
                            </option>
                        )
                      })}
                    </select>
                  </div>
                  <div className="form-floating mb-3">
                    <textarea value={reason} onChange={handleReasonChange} id="reason" rows="3" name="reason" className="form-control"></textarea>
                    <label htmlFor="reason">Reason</label>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        </div>
        </>
        );
}

export default AppointmentForm;
