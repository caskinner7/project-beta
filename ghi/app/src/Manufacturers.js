import React, { useEffect, useState } from 'react';


function Manufacturers() {
    const [manufacturerState, setManufacturerState] = useState([]);

    const loadManufacturers = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/')
        if (response.ok) {
            const data = await response.json();
            setManufacturerState(data.manufacturers)
        } else {
            console.error(response)
        }
    }

    useEffect(() => {
        loadManufacturers();
    }, []);

    return (
        <>
          <h1>Manufacturers</h1>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
              </tr>
            </thead>
            <tbody>
              {manufacturerState.map(manufacturer => {
                return (
                  <tr key={manufacturer.id}>
                    <td>{manufacturer.name}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </>
      );
}

export default Manufacturers;
