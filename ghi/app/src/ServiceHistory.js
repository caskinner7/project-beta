import React, { useEffect, useState } from 'react';


function ServiceHistory(){

  const [appointmentState, setAppointmentState] = useState([]);
  const [vinState, setVinState] = useState([]);
  const [autoState, setAutoState] = useState([]);
  const [searchState, setSearchState] = useState([]);

  const loadAppointments = async () => {
      const response = await fetch('http://localhost:8080/api/appointments/');
      if (response.ok){
          const data = await response.json();
          setAppointmentState(data.appointments)
      } else {
          console.error(response)
      }
  }

  useEffect(()=>{
      loadAppointments();
  }, []);

  const handleVinChange = (event) => {
        const value = event.target.value;
        setVinState(value);}

  const getFilteredItems = (query, items)=>{
      if (!query){
          return items;
      } else {
          return items.filter((appointment) => appointment.vin.includes(query));
      }
  }

  const handleSearch = () => {
    const value = vinState;
    setSearchState(value);
  }

  const filteredItems = getFilteredItems(searchState, appointmentState);

  const loadAutos = async () => {
      const response = await fetch('http://localhost:8100/api/automobiles/');
      if (response.ok){
          const data = await response.json();
          setAutoState(data.autos)
      } else {
          console.error(response)
      }
  }

  useEffect(()=>{
      loadAutos();
  }, []);

  const vinList = [];
  autoState.map(auto => {
    vinList.push(auto.vin);
  })

return (
  <>
      <div>
        <label htmlFor="site-search">Search by vin: </label>
        <input value={vinState} onChange={handleVinChange} type="search" id="site-search" name="q" />
      <button onClick={handleSearch} >Search</button>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {filteredItems.map(appt => {
            return (
              <tr key={appt.id}>
                <td>{appt.vin}</td>
                {
                vinList.includes(appt.vin)
                ? <td>Yes</td>
                : <td>No</td>
                }
                <td>{appt.customer}</td>
                <td>{appt.date_time.slice(0, 10)}</td>
                <td>{appt.date_time.slice(11,16)}</td>
                <td>{appt.technician}</td>
                <td>{appt.reason}</td>
                <td>{appt.status}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
  </>
);
}


export default ServiceHistory;
