import React, {useEffect, useState} from 'react';

function ManufacturerForm() {
  const [manufacturerName, setManufacturerName] = useState([]);

    const handleSubmit = async(event)=>{
        event.preventDefault();
        const data = {};
        data.name = manufacturerName

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            header: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok){
            setManufacturerName('');
        }
    }

    const handleManufacturerNameChange = (event)=>{
        const value = event.target.value;
        setManufacturerName(value);
    }

    return(
        <>
        <div className="container">
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a manufacturer</h1>
                <form onSubmit={handleSubmit} id="create-manufacturer-form">
                  <div className="form-floating mb-3">
                    <input value={manufacturerName} onChange={handleManufacturerNameChange} placeholder="Style Name" required type="text" id="style_name" name="style_name" className="form-control"/>
                    <label htmlFor="style_name">Manufacturer Name</label>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        </div>
        </>
        );
}

export default ManufacturerForm;
