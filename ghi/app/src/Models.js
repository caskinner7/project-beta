import React, { useEffect, useState } from 'react';

function Models() {
    const [modelState, setModelState] = useState([]);

    const loadModels = async () => {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok){
            const data = await response.json();
            setModelState(data.models);
        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        loadModels();
    }, []);
    return (
        <>
          <h1>Models</h1>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Manufacturer</th>
                <th>Picture</th>
              </tr>
            </thead>
            <tbody>
              {modelState.map(model => {
                const imgWidth = {
                    width: "18rem"
                }
                return (
                  <tr key={model.href}>
                    <td>{model.name}</td>
                    <td>{model.manufacturer.name}</td>
                    <td><img style={imgWidth} src={model.picture_url}/></td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </>
      );
}

export default Models;
