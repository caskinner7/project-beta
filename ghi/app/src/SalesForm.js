import React, { useEffect, useState } from 'react';

function SalesForm() {
    const [automobile, setAutomobile] = useState('');
    const [salesperson, setSalesperson] = useState('');
    const [customer, setCustomer] = useState('');
    const [automobiles, setAutomobiles] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [salespeople, setSalespeople] = useState([]);

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }

    const handleSalesPersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const [price, setPrice] = useState('');
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }


    const fetchAutomobiles = async () => {

        const automobileUrl = 'http://localhost:8100/api/automobiles/';

        const response = await fetch(automobileUrl);

        if (response.ok) {
            const automobileData = await response.json();
            setAutomobiles(automobileData.autos)
        }
    }

    const fetchSalespeople = async () => {

        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';

        const response = await fetch(salespeopleUrl);

        if (response.ok) {
            const salespeopleData = await response.json();
            setSalespeople(salespeopleData.salespeople)
        }
    }

    const fetchCustomers = async () => {

        const customersUrl = 'http://localhost:8090/api/customers/';

        const response = await fetch(customersUrl);

        if (response.ok) {
            const customersData = await response.json();
            setCustomers(customersData.customers)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.automobile = automobile;
        data.salesperson = salesperson;
        data.customer = customer;
        data.price = price

        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const autoUrl = `http://localhost:8100/api/automobiles/${automobile}/`;
        const fetchConfig1 = {
            method: "put",
            body: JSON.stringify({ sold: true }),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(salesUrl, fetchConfig);
        const response1 = await fetch(autoUrl, fetchConfig1);

        if ((response.ok) && (response1.ok)) {
            setAutomobile('');
            setSalesperson('');
            setCustomer('');
            setPrice('');

        }

    }


    const sold = (status) => {
        if (status === false) {
            return ("False")
        } else {
            return ("True")
        }
    }

    useEffect(() => {
        fetchAutomobiles();
    }, []);

    useEffect(() => {
        fetchCustomers();
    }, []);

    useEffect(() => {
        fetchSalespeople();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale!</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="mb-3">
                            <select onChange={handleAutomobileChange} value={automobile} required name="automobile" className="form-select">
                                <option value="">Choose an automobile</option>
                                {automobiles.map(auto => {
                                    if (auto.sold === false) {
                                        return (
                                            <option key={auto.vin} value={auto.vin}>
                                                {auto.vin}
                                            </option>
                                        );
                                    }
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleSalesPersonChange} value={salesperson} required name="sales_person" className="form-select">
                                <option value="">Choose a sales person</option>
                                {salespeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.id} value={salesperson.id}>
                                            {salesperson.first_name} {salesperson.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleCustomerChange} value={customer} required name="customer" className="form-select">
                                <option value="">Choose a customer</option>
                                {customers.map(cust => {
                                    return (
                                        <option key={cust.id} value={cust.id}>
                                            {cust.first_name} {cust.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePriceChange} value={price} placeholder="Price" required type="text" name="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalesForm;
