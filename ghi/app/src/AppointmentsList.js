import React, { useEffect, useState } from 'react';


function AppointmentsList(){
    const [appointmentState, setAppointmentState] = useState([]);
    const [cancel, setCancel] = useState('');
    const [autoState, setAutoState] = useState([]);

    const loadAppointments = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok){
            const data = await response.json();
            setAppointmentState(data.appointments)
        } else {
            console.error(response)
        }
    }

    useEffect(()=>{
        loadAppointments();
    }, []);

    const handleCancel = async(appointmentId)=> {
      const data={};
      data.status = "canceled"
      const appointmentUrl = `http://localhost:8080/api/appointments/${appointmentId}/cancel/`;
      const fetchConfig = {
          method: "put",
          body: JSON.stringify(data),
          header: {
              'Content-Type': 'applications/json'
          },
      };

      const response = await fetch(appointmentUrl, fetchConfig);
      if (response.ok){
          loadAppointments();
      }
  }

  const handleFinish = async(appointmentId)=> {
      const data={};
      data.status = "finished"
      const appointmentUrl = `http://localhost:8080/api/appointments/${appointmentId}/finish/`;
      const fetchConfig = {
          method: "put",
          body: JSON.stringify(data),
          header: {
              'Content-Type': 'applications/json'
          },
      };

      const response = await fetch(appointmentUrl, fetchConfig);
      if (response.ok){
          loadAppointments();
      }
  }

    const loadAutos = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok){
            const data = await response.json();
            setAutoState(data.autos)
        } else {
            console.error(response)
        }
    }

    useEffect(()=>{
        loadAutos();
    }, []);

    const vinList = [];
    autoState.map(auto => {
      vinList.push(auto.vin);
    })

    return (
        <>
        <h1>Service Appointments</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Vin</th>
              <th>Is VIP?</th>
              <th>Customer</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
            </tr>
          </thead>
          <tbody>
            {appointmentState.map(appt => {
                if (appt.status === 'created'){
              return (
                <tr key={appt.id}>
                  <td>{appt.vin}</td>
                  {
                  vinList.includes(appt.vin)
                  ? <td>Yes</td>
                  : <td>No</td>
                  }
                  <td>{appt.customer}</td>
                  <td>{appt.date_time.slice(0, 10)}</td>
                  <td>{appt.date_time.slice(11,16)}</td>
                  <td>{appt.technician}</td>
                  <td>{appt.reason}</td>
                  <td>
                  <button value={cancel} onChange={handleCancel} onClick={()=>handleCancel(appt.id)} type="button" className="btn btn-danger">Cancel</button>
                  <button onClick={()=> handleFinish(appt.id)} type="button" className="btn btn-success">Finish</button>
                  </td>
                </tr>
              );
            }
            })}
          </tbody>
        </table>
    </>
    );
}

export default AppointmentsList;
